require 'rails_helper'

RSpec.describe "xsers/show", type: :view do
  before(:each) do
    @xser = assign(:xser, Xser.create!(
      :user_name => "User Name",
      :email => "Email",
      :is_super_user => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/User Name/)
    expect(rendered).to match(/Email/)
    expect(rendered).to match(/false/)
  end
end
