require 'rails_helper'

RSpec.describe "xsers/edit", type: :view do
  before(:each) do
    @xser = assign(:xser, Xser.create!(
      :user_name => "MyString",
      :email => "MyString",
      :is_super_user => false
    ))
  end

  it "renders the edit xser form" do
    render

    assert_select "form[action=?][method=?]", xser_path(@xser), "post" do

      assert_select "input#xser_user_name[name=?]", "xser[user_name]"

      assert_select "input#xser_email[name=?]", "xser[email]"

      assert_select "input#xser_is_super_user[name=?]", "xser[is_super_user]"
    end
  end
end
