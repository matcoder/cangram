require 'rails_helper'

RSpec.describe "xsers/new", type: :view do
  before(:each) do
    assign(:xser, Xser.new(
      :user_name => "MyString",
      :email => "MyString",
      :is_super_user => false
    ))
  end

  it "renders new xser form" do
    render

    assert_select "form[action=?][method=?]", xsers_path, "post" do

      assert_select "input#xser_user_name[name=?]", "xser[user_name]"

      assert_select "input#xser_email[name=?]", "xser[email]"

      assert_select "input#xser_is_super_user[name=?]", "xser[is_super_user]"
    end
  end
end
