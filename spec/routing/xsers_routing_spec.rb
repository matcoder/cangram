require "rails_helper"

RSpec.describe XsersController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/xsers").to route_to("xsers#index")
    end

    it "routes to #new" do
      expect(:get => "/xsers/new").to route_to("xsers#new")
    end

    it "routes to #show" do
      expect(:get => "/xsers/1").to route_to("xsers#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/xsers/1/edit").to route_to("xsers#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/xsers").to route_to("xsers#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/xsers/1").to route_to("xsers#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/xsers/1").to route_to("xsers#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/xsers/1").to route_to("xsers#destroy", :id => "1")
    end

  end
end
