json.extract! user, :id, :user_name, :email, :is_super_user, :created_at, :updated_at
json.url user_url(user, format: :json)
