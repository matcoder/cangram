class UsersController < ApplicationController
  before_action :only_allow_super_user
  before_action :set_user, only: [:show, :edit, :edit_password, :update, :destroy]
  
  # GET /xsers
  # GET /xsers.json
  def index
    @users = User.all
  end

  # GET /xsers/1
  # GET /xsers/1.json
  def show
  end

  # GET /xsers/new
  def new
    @user = User.new
  end

  # GET /xsers/1/edit
  def edit
  end

  # GET /xsers/1/edit
  def edit_password
  end

  # POST /xsers
  # POST /xsers.json
  def create
    @user = User.new(xser_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /xsers/1
  # PATCH/PUT /xsers/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /xsers/1
  # DELETE /xsers/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    def only_allow_super_user
      authenticate_user!
      redirect_to root_path, :notice => "Not allowed!" and return unless current_user.is_super_user || (@user && (@user == current_user))
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:user_name, :email, :is_super_user, :password, :password_confirmation)
    end
end
