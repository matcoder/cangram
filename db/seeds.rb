# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

u = User.create!( {user_name: 'SuperUser', email: 'superuser@cangram.edu', password: 'pass1234', password_confirmation: 'pass1234', is_super_user: true})
puts "created super user:"
puts u.inspect