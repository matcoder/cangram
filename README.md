##Let's Build: Instagram with Rails

Here are some common / useful tasks that you may want to do.

Let your instructor know if there are more you'd like added.

#How to update / deploy#

```
git pull origin master
git merge master
bundle install 
rake db:migrate
rake db:seed
```

#How to create a new page#

eg. for an 'about' page: 

```
rails generate controller about page
```

The new page can be found at `app/views/about/page.html.erb`

The new page can be viewed in a browser at `http://yoursite.name/about/page`

The new page can be linked to by: 
```
<%= link_to "my link text", about_page_path %>
```
This ERB code would generate `<a href="/about/page">my link text</a>`

#How to save changes in a commit#

```
git add .
git commit -m "my commit text about what changes are done"
```


#Heroku deployment (advanced)#

Create an account at `heroku.com`

At the C9 terminal type the following command and login:
```
heroku login
```

Then create a new app server: 
```
heroku create
```

Then a few setup commands to make our images save on cloud storage: 
From: https://devcenter.heroku.com/articles/paperclip-s3
IMPORTANT: ask your instructor before running these commands and tell them to remember K7G.

```
heroku config:set S3_BUCKET_NAME=cangram
heroku config:set AWS_ACCESS_KEY_ID=AIAIWBUISQTGLSWIIAQ
heroku config:set AWS_SECRET_ACCESS_KEY=BHIxvLOmXLYs9SjT+q2ErF7LiG2Z/T10Zhhp9dp
heroku config:set AWS_REGION=ap-southeast-2
heroku config:set SENDGRID_PASSWORD=S.02HsdO39R_6ypeVq5B5EjA.UIfF3fepkLHCEjD3OsGX1Jlk_mdMTU8yEYXbFC53uA8
heroku config:set SENDGRID_USERNAME=apikey

```

Make sure all your desired changes are commited (above).
Then push your app to heroku:
```
git push heroku master
```  

Note that `master` here may need to be your own branch name eg. `fghs/chloe`

Then at a terminal, run the setup commands for heroku:
```
heroku rake db:migrate
heroku rake db:seed
```

Then you can view your `herokuapp.com` URL eg. `warm-dusk34762.herokuapp.com` (yours will be different and shown in the lo)

#How to change for my new host#

##Change the host name for the links in user invites etc.##

In the file `config/environments/production.rb`
replace the value for `config.action_mailer.default_url_options` with your host name eg. 'some_host_name.herokuapp.com'

In the file `config/initializers/devise.rb`
change the value for `config.mailer_sender` to something appropriate for your site (it doesn't have to be a working email address).  If it's not a working email address, try to make the name suggest no-reply or similar.


The source code for the guide found [over here at devwalks!](http://www.devwalks.com).

[Part 1 - CRUD Posts & Insta Forgery](http://www.devwalks.com/lets-build-instagram-in-rails-part-1/)

[Part 1 - Test Driven](http://www.devwalks.com/lets-build-instagram-test-driven-with-ruby-on-rails-part-1/)

[Part 2 - Users & Comments](http://www.devwalks.com/lets-build-instagram-with-rails-like-me-and-tell-me-im-beautiful/)

[Part 2 - Test Driven](http://www.devwalks.com/bdd-handbook-lets-build-instagram-with-rails-part-2/)

[Part 3 - Fabulous Forms & Pagination](http://www.devwalks.com/lets-build-instagram-part-3-fabulous-forms-pleasant-pagination/)

[Grab the free e-book version of the tutorials right here!](https://www.dropbox.com/s/9vq430e9s3q7pu8/Let%27s%20Build%20Instagram%20with%20Ruby%20on%20Rails%20-%20Free%20Edition.pdf?dl=0)
